import { injectGlobal } from 'styled-components'
import style from './theme'
// NOTE: Styles of container elements duplicated to make SSR version without css looks prettier
injectGlobal`
  body {
    margin: 0;
	  padding: 0;
	  overflow-x: hidden;
	  min-width: 320px;
	  background: white;
	  font-family: 'Lato', 'Helvetica Neue', Arial, Helvetica, sans-serif;
	  font-size: 14px;
	  line-height: 1.4285em;
	  color: rgba(0, 0, 0, 0.87);
  }

  .pushable {
	  height: 100%;
	  overflow-x: hidden;
	  padding: 0;
    transform: translate3d(0, 0, 0);
	}
	
	.ui.form {
		textarea {
			border: 1px solid #989494;
		}
		textarea:hover {
			box-shadow: inset 0 0 0 0 ${style.primaryColorLight},
			0 2px 1px 0 ${style.primaryColorLight};
			border: 1px solid ${style.primaryColorDark};
		}
		textarea:focus {
			box-shadow: inset 0 0 0 0 ${style.primaryColorDark},
			0 2px 1px 0 ${style.primaryColorDark};
			border: 1px solid ${style.primaryColorDark};
		}
		.field {
			.ui.input {
				input {
					border: 1px solid #989494;
				}
				input:hover {
					box-shadow: inset 0 0 0 0 ${style.primaryColorLight},
					0 2px 1px 0 ${style.primaryColorLight};
					border: 1px solid ${style.primaryColorDark};
				}
				input:focus {
					box-shadow: inset 0 0 0 0 ${style.primaryColorDark},
					0 2px 1px 0 ${style.primaryColorDark};
					border: 1px solid ${style.primaryColorDark};
				}
			}
		}
	}

	.ui.icon.input {
		input {
			border: 1px solid #989494;
		}
		input:hover {
			box-shadow: inset 0 0 0 0 ${style.primaryColorLight},
			0 2px 1px 0 ${style.primaryColorLight};
			border: 1px solid ${style.primaryColorDark};
		}
		input:focus {
			box-shadow: inset 0 0 0 0 ${style.primaryColorDark},
			0 2px 1px 0 ${style.primaryColorDark};
			border: 1px solid ${style.primaryColorDark};
		}
	}

  #app {
    width: 100%;
    height: 100%;
	}

  .wrapper {
  	width: 100%;
  	height: fit-content;
		margin: 0 auto;
	}

	.content-inner {
		padding: 24px;
		background: #ffffff;
		box-shadow: @shadow-1;
	}
`
