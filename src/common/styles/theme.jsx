/**
 * You know where to get a new material palette, right?:
 * {@link https://www.materialpalette.com/}
 */
export default {
    primaryColorDark: '#388E3C',
    primaryColor: '#4CAF50',
    primaryColorLight: '#C8E6C9',
    primaryColorText: '#000000',
    accentColor: '#009688',
    primaryBackgroundColor: '#ffffff',
    secondaryTextColor: '#757575',
    dividerColor: '#BDBDBD'
}
