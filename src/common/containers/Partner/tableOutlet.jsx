import _ from 'lodash'
import React, { Component } from 'react'
import { Table, Pagination } from 'semantic-ui-react'
// import ReactTable from 'react-table'

// const tableData = [
//     {
//         name: 'aaaa assd',
//         age: 26,
//         friend: {
//             name: 'addghtgdfhf assss',
//             age: 23
//         }
//     }
// ]
const tableData = [
    {
        name: 'aaaa assd',
        age: 26,
        gender: 'male'
    }
]
class TableExampleSortable extends Component {
    state = {
        column: null,
        data: tableData,
        direction: null
    }

    handleSort = clickedColumn => () => {
        const { column, data, direction } = this.state

        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending'
            })

            return
        }

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending'
        })
    }

    render () {
        const { data } = this.state
        // const columns = [
        // 	{
        // 		Header: 'Name',
        // 		accessor: 'name'
        // 	}, {
        // 		Header: 'Age',
        // 		accessor: 'age',
        // 		Cell: props => <div className='number'>{props.value}</div>
        // 	}, {
        // 		id: 'friendName',
        // 		Header: 'Friend Name',
        // 		accessor: d => d.friend.name
        // 	}, {
        // 		Header: props => <div>Friend Age</div>,
        // 		accessor: 'friend.age'
        // 	}
        // ]

        return (
            <div>
                {/* <ReactTable style={{ height: 'fit-content', maxHeight: '350px' }} data={data} defaultPageSize={5} columns={columns} /> */}
                <Table
                    singleLine
                    pagination
                >
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Registration Date</Table.HeaderCell>
                            <Table.HeaderCell>E-mail address</Table.HeaderCell>
                            <Table.HeaderCell>Premium Plan</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {_.map(data, ({ age, gender, name }) => (
                            <Table.Row key={name}>
                                <Table.Cell>{name}</Table.Cell>
                                <Table.Cell>{age}</Table.Cell>
                                <Table.Cell>{gender}</Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>
                <Pagination defaultActivePage={5} totalPages={10} />
            </div>
        )
    }
}

export default TableExampleSortable
