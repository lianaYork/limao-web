import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Header } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl'
import { FormInput } from './style'
// import ImageUploader from 'components/cdn/ImageUploader'
// import ImageShow from 'components/cdn/ImageShow'

class FormExampleCaptureValues extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = (evt) => {
        evt.preventDefault()
        // pass the input field value to the event handler passed
        // as a prop by the parent (App)
        this.props.handlerFromParant(this.state)
    }
    render () {
        return (
            <div>
                <div>
                    <FormattedMessage id="Company.header">
                        {title => <Header as="h1" style={{ textAlign: 'center' }}>{title}</Header>}
                    </FormattedMessage>
                    <Form onSubmit={this.handleSubmit}>
                        <FormattedMessage id="Company.brand">
                            {title => <FormInput label={title} placeholder={title} name='brand' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Company.type">
                            {title => <FormInput label={title} placeholder={title} name='companyType' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Company.name">
                            {title => <FormInput label={title} placeholder={title} name='companyName' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <Form.Field>
                            <FormattedMessage id="Company.phone">
                                {title => <FormInput type='number' label={title} placeholder={title} name='companyPhone' onChange={this.handleChange} />}
                            </FormattedMessage>
                        </Form.Field>
                        <FormattedMessage id="Company.idNo">
                            {title => <FormInput label={title} placeholder={title} name='companyIdNo' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Company.address">
                            {title => <FormInput label={title} placeholder={title} name='companyAddress' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Company.taxId">
                            {title => <FormInput label={title} placeholder={title} name='companyTaxId' onChange={this.handleChange} />}
                        </FormattedMessage>
                        {/* <ImageUploader />
                        <ImageShow /> */}
                        <Form.Button content='Submit' />
                    </Form>
                </div>
            </div>
        )
    }
}

FormExampleCaptureValues.propTypes = {
    name: PropTypes.string,
    email: PropTypes.string,
    handlerFromParant: PropTypes.func
}

export default FormExampleCaptureValues
