// @flow
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet'
import { withRouter } from 'react-router'
import { Grid, Header, Menu, Tab } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl'
import { SearchBannerComponent, BannerBackground } from './style'
import HeaderText from 'components/parts/HeaderText/index'
import { getLayoutMobileStatuses } from 'selectors'
import FormPersonal from './formPersonal'
import FormCompany from './formCompany'
import TableOutlet from './tableOutlet'

type Props = {
    isMobile: boolean
}

class Partner extends Component<Props> {
    constructor () {
        super()
        this.handleData = this.handleData.bind(this)
        this.changeForm = this.changeForm.bind(this)
        this.state = {
            data: {},
            form: '1',
            panelist: [
                {
                    key: 'Owner.header',
                    item: <Tab.Pane attached={false}><FormPersonal handlerFromParant={this.handleData} /></Tab.Pane>
                },
                {
                    key: 'Company.header',
                    item: <Tab.Pane attached={false}><FormCompany handlerFromParant={this.handleData} /></Tab.Pane>
                },
                {
                    key: 'Outlet.header',
                    item: <Tab.Pane attached={false}><TableOutlet style={{ width: 'fit-content' }} /></Tab.Pane>
                }
            ],
            panes: []
        }
    }
    props: Props
    handleData (data) {
        this.setState({
            data
        })
    }
    changeForm (key) {
        this.setState({
            form: key
        })
    }
    componentWillMount () {
        let panes = []
        const { panelist } = this.state
        for (let id = 0; id < panelist.length; id += 1) {
            panes.push({
                menuItem:
                    (<Menu.Item key={panelist[id].key.toString()}>
                        <FormattedMessage id={panelist[id].key.toString()}>
                            {header => <Header as="h5" style={{ textAlign: 'center' }}>{header}</Header>}
                        </FormattedMessage>
                    </Menu.Item>),
                render: () => panelist[id].item
            })
        }
        this.setState({ panes })
    }

    render () {
        const { panes, panelist, ...other } = this.state
        const { isMobile } = this.props
        return (
            <div>
                <div className="wrapper">
                    <Helmet>
                        <title>All you can eat</title>
                    </Helmet>
                    <Grid stackable columns={2}>
                        <Grid.Row centered>
                            <Grid.Column width={16}>
                                <SearchBannerComponent>
                                    <BannerBackground>
                                        <HeaderText.Header1 as="h1" style={{ fontSize: '3em' }}>
                                            <FormattedMessage id="Dashboard.h1" />
                                        </HeaderText.Header1>
                                    </BannerBackground>
                                    <BannerBackground>
                                        <Header sub as="h3" style={{ fontSize: '1em' }}>
                                            <FormattedMessage id="Partner.h3-1" />
                                        </Header>
                                    </BannerBackground>
                                </SearchBannerComponent>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
                <div className="content-inner">
                    <Grid stackable centered columns={2}>
                        <Grid.Row>
                            <Grid.Column>
                                {isMobile ? <Tab menu={{ attached: 'top' }} panes={panes} /> : <Tab menu={{ fluid: true, vertical: true, tabular: 'right' }} panes={panes} />}
                                <pre>{JSON.stringify({ ...other }, null, 2)}</pre>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const { isMobile } = getLayoutMobileStatuses(state)
    return {
        isMobile
    }
}

Partner.propTypes = {
    changeForm: PropTypes.func
}

export default withRouter(connect(mapStateToProps)(Partner))
