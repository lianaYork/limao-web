import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Header } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl'
import { FormInput } from './style'

class FormExampleCaptureValues extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = (evt) => {
        evt.preventDefault()
        // pass the input field value to the event handler passed
        // as a prop by the parent (App)
        this.props.handlerFromParant(this.state)
    }
    render () {
        return (
            <div>
                <div>
                    <FormattedMessage id="Owner.header">
                        {title => <Header as="h1" style={{ textAlign: 'center' }}>{title}</Header>}
                    </FormattedMessage>
                    <Form onSubmit={this.handleSubmit}>
                        <FormattedMessage id="Owner.name">
                            {title => <FormInput label={title} placeholder={title} name='ownerName' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Owner.phone">
                            {title => <FormInput label={title} placeholder={title} name='personalPhone' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Owner.email">
                            {title => <FormInput label={title} placeholder={title} name='email' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <Form.Group>
                            <FormattedMessage id="Owner.idType">
                                {title => <FormInput style={{ width: '100%' }} label={title} placeholder={title} name='idType' onChange={this.handleChange} />}
                            </FormattedMessage>
                            <FormattedMessage id="Owner.idNo">
                                {title => <FormInput style={{ width: '100%' }} label={title} placeholder={title} name='idNo' onChange={this.handleChange} />}
                            </FormattedMessage>
                        </Form.Group>
                        <FormattedMessage id="Owner.address">
                            {title => <Form.Field><Form.TextArea style={{ border: '1px solid #989494' }} rows={2} label={title} placeholder={title} name='address' onChange={this.handleChange} /></Form.Field>}
                        </FormattedMessage>
                        <FormattedMessage id="Owner.taxId">
                            {title => <FormInput label={title} placeholder={title} name='taxId' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Owner.accBank">
                            {title => <FormInput label={title} placeholder={title} name='accBank' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Owner.accNo">
                            {title => <FormInput label={title} placeholder={title} name='accNo' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <FormattedMessage id="Owner.accName">
                            {title => <FormInput label={title} placeholder={title} name='accName' onChange={this.handleChange} />}
                        </FormattedMessage>
                        <Form.Button content='Submit' />
                    </Form>
                </div>
            </div>
        )
    }
}

FormExampleCaptureValues.propTypes = {
    name: PropTypes.string,
    email: PropTypes.string,
    handlerFromParant: PropTypes.func
}

export default FormExampleCaptureValues
