import styled from 'styled-components'
// import imageBanner from '../../../../static/images/Food-Tips-Front-Page.png'
import { media } from 'styles/utils'

const url = 'http://res.cloudinary.com/lianayork/image/upload/v1518235527/limaoWeb/h8vofsldr7lrctuo3a5h.jpg'

export const SearchBannerComponent = styled.div`
  display: grid;
  flex-grow: 1;
  align-items: center;
  background-image: url(${url});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;
  width: 100%;
  min-height: 200px;
  height: fit-content;
  padding: 20px;
  text-align:center; /*centers horizontally*/
  line-height:300px; 
  ${media.md`
    background-size: auto;
  `}
`

export const SearchContainer = styled.div`
background-color: #018c0091;
display: grid;
flex-grow: 1;
margin: 0px 0 5px 0;
align-items: center;
background-size: 100%;
width: 100%;
opacity: 0.6;
height: auto;
min-height: 70px;
// margin: auto;
text-align:center; /*centers horizontally*/
`
export const BannerBackground = styled.div`
  margin: auto;
  width: fit-content;
  background-color: #ffffff;
  padding: 10px;
  border-radius: 12px;  
  &:hover {
    transform: scale(1.1); 
  }
`
