// @flow
import React from 'react'
import { Helmet } from 'react-helmet'
import { Grid, Header, Search as SearchComponent } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl'
import { SearchBannerComponent, SearchContainer, BannerBackground } from './style'
import ImageComponent from '../../components/parts/ImageItem/index'
import ImageSource from '../../../../static/images/Logo.png'
const Home = () => {
    return (
        <div>
            <div className="wrapper">
                <Helmet>
                    <title>All you can eat</title>
                </Helmet>
                <Grid stackable columns={3}>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SearchBannerComponent>
                                <BannerBackground>
                                    <Header as="h1" style={{ fontSize: '3em' }}>
                                        <FormattedMessage id="Dashboard.h1" />
                                    </Header>
                                </BannerBackground>
                                <BannerBackground>
                                    <Header as="h2">
                                        <FormattedMessage style={{ fontSize: '1.5em' }} id="Dashboard.h2-1" />
                                    </Header>
                                </BannerBackground>
                            </SearchBannerComponent>
                            <SearchContainer>
                                <label htmlFor="user-search">
                                    Explore
                                </label>
                                <SearchComponent id="user-search" label="Explore new taste" style={{ textAlign: 'center' }} noResultsMessage="" />
                            </SearchContainer>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
            <div className="content-inner">
                <Grid stackable columns={3}>
                    <Grid.Row>
                        <Grid.Column>
                            <ImageComponent.SegmentItem>
                                <ImageComponent.ImageItem alt="logo" src={ImageSource} />
                            </ImageComponent.SegmentItem>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </div>
    )
}

export default Home
