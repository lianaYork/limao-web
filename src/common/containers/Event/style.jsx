import styled from 'styled-components'
// import imageBanner from '../../../../static/images/Food-Tips-Front-Page.png'
import { Form } from 'semantic-ui-react'
import { media } from 'styles/utils'

const url = 'http://res.cloudinary.com/lianayork/image/upload/v1518235527/limaoWeb/h8vofsldr7lrctuo3a5h.jpg'

export const SearchBannerComponent = styled.div`
  display: grid;
  flex-grow: 1;
  align-items: center;
  background-image: url(${url});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;
  width: 100%;
  min-height: 200px;
  height: fit-content;
  padding: 20px;
  text-align:center; /*centers horizontally*/
  line-height:300px; 
  ${media.md`
    background-size: auto;
  `}
`
export const BannerBackground = styled.div`
  margin: auto;
  width: fit-content;
  background-color: #ffffff;
  padding: 10px;
  border-radius: 12px;  
  &:hover {
    transform: scale(1.1); 
  }
`
export const FormInput = styled(Form.Input) `
  display: block;
  align-items: center;
  &#.ui.form .fields .field .ui.input input {
		border: 1px solid black;
  };
  text-align: left;
`
