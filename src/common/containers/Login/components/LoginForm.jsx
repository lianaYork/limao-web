// @flow
import React, { Component } from 'react'
import {
    Form,
    Message,
    Button,
    Label,
    Input as InputComponent
} from 'semantic-ui-react'
import { login } from '../../../../services/auth'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import { LOGIN_AUTH } from 'actions/auth'
import type { FormProps } from 'redux-form'

type Props = {
    login: (data: Object) => void
} & FormProps

const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined

const InputField = ({
    input,
    label,
    labelText = null,
    required,
    meta: { touched, error },
    ...rest
}: any) => (
    <Form.Field error={!!(touched && error)} required={required}>
        <label htmlFor={rest.id || rest.name || ''}>{label}</label>
        <InputComponent
            id={rest.id || rest.name || ''}
            label={labelText}
            required={required}
            {...input}
            {...rest}
        />
        {touched && error ? (
            <Label basic color="red" pointing>
                {error}
            </Label>
        ) : null}
    </Form.Field>
)

class LoginComponent extends Component<Props, State> {
    componentWillMount () {
        async function makeRequest (params) {
            var result = await login(credential).catch(error => { console.log('error', error) })
            if (result.success) {
                return result
            }
            throw result
            // expected output: "resolved"
        }

        const credential = {
            email: 'veirryau@gmail.com',
            password: 123456
        }
        const data = makeRequest(credential)
        console.log('data', data)
    }
    render () {
        /* By default we use https://github.com/erikras/redux-form
				Probably, you'll need: https://github.com/ckshekhar73/react-semantic-redux-form/blob/master/src/index.js
				(don't install, better copy sources to the project)
        */
        const fields = [
            {
                name: 'non_field_errors',
                component ({ meta: { error } }) {
                    return error ? (
                        <Message error>
                            <Message.Header>{'Login failed :('}</Message.Header>
                            <p>{error}</p>
                        </Message>
                    ) : null
                }
            },
            {
                placeholder: 'Email',
                name: 'email',
                id: 'email',
                label: 'Email',
                component: InputField,
                validate: email
            },
            {
                placeholder: 'Password',
                type: 'password',
                name: 'password',
                id: 'password',
                label: 'Password',
                component: InputField
            }
        ]
        const { handleSubmit, login, invalid, submitting } = this.props
        let form = []
        fields.map((a, i) => form.push(<Field key={i} {...a} />))
        return (
            <Form onSubmit={handleSubmit(login)} error={invalid}>
                {/* {fields.map((a, i) => <Field key={i} {...a} />)} */}
                {form}
                <div style={{ textAlign: 'center' }}>
                    <Button content="Login" icon="sign in" loading={submitting} />
                </div>
            </Form>
        )
    }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
    async login (data) {
        return dispatch(LOGIN_AUTH(data))
    }
})

export default reduxForm({ form: 'LOGIN_FORM' })(
    connect(mapStateToProps, mapDispatchToProps)(LoginComponent)
)
