import styled from 'styled-components'
import {Image, Segment} from 'semantic-ui-react'

export const ImageItem = styled(Image)`
    width: 100%;
    height: auto;
`

export const SegmentItem = styled(Segment)`
    width: 200px;
    height: 200px;
    border: 1px solid black;
`
