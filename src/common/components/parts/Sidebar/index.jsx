// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Menu, Icon } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { LOGOUT_AUTH } from 'actions/auth'
import { getSidebarRoutes } from 'routing'
import { getLayoutState, getLayoutMobileStatuses } from 'selectors'
import {
    StyledSidebar,
    SidebarLogo,
    SidebarLogoContainer,
    SidebarItem,
    SidebarLogoutItem
} from './style'
import type { RouteItem } from 'types'

type Props = {
    open: boolean,
    logout: () => void,
    isMobile: boolean
}

class SidebarComponent extends Component<Props> {
    props: Props

    render () {
        const { open, logout } = this.props
        const routing: RouteItem[] = getSidebarRoutes()

        const sidebarProps = {
            visible: open,
            as: Menu,
            vertical: true,
            direction: 'left',
            icon: 'labeled',
            animation: 'push',
            width: 'thin',
            inverted: true
        }

        const routes = routing.map((route, i) => {
            const { external, path, strict, exact, meta } = route
            const { icon, name } = meta
            // Props that are common for both "<a>" and "RR Link"
            const basicProps = {
                as: external ? 'a' : NavLink,
                link: true,
                [external ? 'href' : 'to']: path
            }

            // Is it's RR Link, then it needs additional props
            const externalProps = external
                ? {}
                : {
                    strict,
                    exact,
                    activeClassName: 'active'
                }

            // Summarize
            const propsMenuItem = {
                ...externalProps,
                ...basicProps
            }

            return (
                <SidebarItem key={i} {...propsMenuItem} icon>
                    <Icon name={icon} /> {name}
                </SidebarItem>
            )
        })

        return (
            <StyledSidebar {...sidebarProps}>
                <SidebarLogoContainer>
                    <SidebarLogo alt="logo" shape="circular" floated="left" avatar={true} />
                </SidebarLogoContainer>
                {routes}
                <SidebarLogoutItem onClick={logout}>
                    <Icon name="sign out" />
                    Logout
                </SidebarLogoutItem>
            </StyledSidebar>
        )
    }
}

const mapStateToProps = state => {
    const { sidebarOpened } = getLayoutState(state)
    const { isMobile } = getLayoutMobileStatuses(state)

    return {
        open: sidebarOpened,
        isMobile
    }
}

const mapDispatchToProps = dispatch => ({
    logout () {
        dispatch(LOGOUT_AUTH())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(SidebarComponent)
