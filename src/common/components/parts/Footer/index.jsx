/**
 * @flow
 */
import React from 'react'
import { Grid, Header } from 'semantic-ui-react'
import { StyledFooter, StyledFooterInner } from './style'

const Footer = () => {
    return (
        <StyledFooter>
            <StyledFooterInner>
                <Grid relaxed>
                    <Grid.Row verticalAlign="middle">
                        <Grid.Column width={12} mobile={16}>
                            <Header as="h5">
                                <Header.Content style={{ textColor: '#000000' }}>
                                    © 2018 Limao. All right reserved
                                </Header.Content>
                            </Header>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </StyledFooterInner>
        </StyledFooter>
    )
}

export default Footer
