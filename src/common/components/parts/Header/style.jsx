import styled from 'styled-components'
import { media } from 'styles/utils'
import { Button } from 'semantic-ui-react'
export const StyledHeader = styled.header`
	background: ${props => props.theme.primaryColor};
	border-bottom: 1px solid ${props => props.theme.primaryColorDark};
	box-shadow: inset 0 0 0 0 ${props => props.theme.primaryColorDark},
		0 2px 1px 0 ${props => props.theme.primaryColorDark};
	color: ${props => props.theme.primaryColorText};
	display: flex;
	justify-content: center;
	flex-direction: column;
	width: 100%;
	z-index: 444;
	height: 50px;
`

export const HeaderInner = styled.div`
	display: flex;
	padding: 0 15px;
`

export const PageTitle = styled.span`
	line-height: 1;
	font-size: 18px;
	align-items: center;
	display: flex;
`

export const Navicon = styled.span`
	width: 48px;
	height: 48px;
	padding: 12px;
	line-height: 1;
	font-size: 24px;
	display: block;
	${media.md`
		display: block;
	`};
`

export const HeaderButton = styled(Button) `
	&#header-button {
		align-self: center;
		box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
		color: ${props => props.theme.primaryColorText}!important;
		background-color: ${props => props.theme.primaryColorDark}!important;
	}
`
export const SearchBannerComponent = styled.div`
  display: grid;
  flex-grow: 1;
  align-items: center;
  background-image: url("https://gatherr.ca/wp-content/uploads/2016/05/Food-Tips-Front-Page.jpg");
  width: 100%;
  height: 300px;
  // margin: auto;
  opacity: 0.6;
  text-align:center; /*centers horizontally*/
  line-height:300px; 
`
