import styled from 'styled-components'
import {media} from 'styles/utils'
import {Header} from 'semantic-ui-react'

export const Header1 = styled(Header)`
    font-size: 5em;
    ${media.md`
        font-size: 2em;
	`};
`
