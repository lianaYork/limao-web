// @flow
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as reduxFormReducer } from 'redux-form'

import type { State as AuthState } from 'reducers/auth'
import type { State as LayoutState } from 'reducers/layout'

import { layout } from './layout'
import { auth } from './auth'

// Root reducer
export default combineReducers({
    layout,
    auth,
    routing: routerReducer,
    form: reduxFormReducer
})

export type GlobalState = { layout: LayoutState } & { auth: AuthState }
