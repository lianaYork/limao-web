// @flow
// Styles
// If you want full SUI CSS:
// import 'semantic-ui-css/semantic.css'
// If you want only some components from SUI:
// import 'react-table/react-table.css'
import 'semantic-ui-css/components/button.min.css'
import 'semantic-ui-css/components/container.min.css'
import 'semantic-ui-css/components/dimmer.min.css'
import 'semantic-ui-css/components/divider.min.css'
import 'semantic-ui-css/components/grid.min.css'
import 'semantic-ui-css/components/table.min.css'
import 'semantic-ui-css/components/header.min.css'
import 'semantic-ui-css/components/form.min.css'
import 'semantic-ui-css/components/icon.min.css'
import 'semantic-ui-css/components/image.min.css'
import 'semantic-ui-css/components/input.min.css'
import 'semantic-ui-css/components/menu.min.css'
import 'semantic-ui-css/components/tab.min.css'
import 'semantic-ui-css/components/label.min.css'
import 'semantic-ui-css/components/list.min.css'
import 'semantic-ui-css/components/loader.min.css'
import 'semantic-ui-css/components/reset.min.css'
import 'semantic-ui-css/components/sidebar.min.css'
import 'semantic-ui-css/components/site.min.css'
// babel polyfill (ie 10-11) + fetch polyfill
import 'babel-polyfill'
import 'isomorphic-fetch'
// Application
import React from 'react'
import { hydrate } from 'react-dom'
import { AsyncComponentProvider } from 'react-async-component'
import asyncBootstrapper from 'react-async-bootstrapper'
import { configureApp, configureRootComponent } from 'common/app'
import { AppContainer } from 'react-hot-loader'
import type { GlobalState } from 'reducers'
import type { i18nConfigObject } from 'types'

if (process.env.NODE_ENV === 'production') {
    require('common/pwa')
}

const initialState: GlobalState = window.__INITIAL_STATE__ || {}
const i18n: i18nConfigObject = window.__I18N__ || {}
const asyncState: Object = window.__ASYNC_STATE__ || {}

const { store, routes, history } = configureApp(initialState)
const RootComponent = configureRootComponent({
    store,
    routes,
    history,
    i18n
})

const app = (
    <AppContainer warnings={false}>
        <AsyncComponentProvider rehydrateState={asyncState}>
            {RootComponent}
        </AsyncComponentProvider>
    </AppContainer>
)

asyncBootstrapper(app).then(() => {
    console.log('__INITIAL_STATE__:', initialState)
    hydrate(app, document.getElementById('app'))
})

if (module.hot) {
    module.hot.accept()
}
