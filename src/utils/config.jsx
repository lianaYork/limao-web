const APIV1 = '/api/'
const APIHOST = 'localhost'
const APIPORT = 8000
const APIURL = `http://${APIHOST}:${APIPORT}`

module.exports = {
    name: 'Limao-web',
    version: 'v0.0.1',
    logo: '/Logo.png',
    CORS: `${APIURL}`,
    apiHeader: {
        'Content-Type': 'application/json'
    },
    api: {
        user: `${APIURL}${APIV1}auth/:id`,
        users: `${APIURL}${APIV1}auth`,
        userLogin: `${APIURL}${APIV1}auth/login`,
        userLogout: `${APIURL}${APIV1}auth/logout`
    }
}
