import Cookies from 'js-cookie'
import { apiHeader } from './config'

const crypto = require('crypto')
const algorithm = 'aes-256-ctr'
const password = new Date().toISOString().slice(0, 10).replace(/-/g, '')

const encrypt = (text, rdmtxt) => {
    const cipher = crypto.createCipher(algorithm, (rdmtxt || '') + password)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return crypted
}

const decrypt = (text, rdmtxt) => {
    try {
        const decipher = crypto.createDecipher(algorithm, (rdmtxt || '') + password)
        let dec = decipher.update(text, 'hex', 'utf8')
        dec += decipher.final('utf8')
        return dec
    } catch (err) {
        return null
    }
}

const apiheader = () => {
    try {
        const idToken = Cookies.get('JWT_TOKEN')
        apiHeader.Authorization = 'Bearer ' + idToken
        return apiHeader
    } catch (err) {
        return null
    }
}

module.exports = {
    encrypt,
    decrypt,
    apiheader
}
