import { request, config, crypt } from '../utils'

const { api } = config
const { users, userLogout, userLogin } = api

export async function login (params) {
    return request({
        url: userLogin,
        method: 'post',
        data: params
    })
}

export async function logout (params) {
    return request({
        url: userLogout,
        method: 'post', // 'get',
        data: params,
        headers: crypt.apiheader()
    })
}

export async function changePw (params) {
    const url = params.id ? users + '/' + params.id : users
    const apiHeaderToken = crypt.apiheader()
    return request({
        url: url,
        method: 'put',
        data: params.data,
        body: params.data,
        headers: apiHeaderToken
    })
}
